const express = require('express');

const {
    getAllSuppliers,
    getOneSupplier,
    createSupplier,
    updateSupplier,
    deleteSupplier,
} = require('../controllers/suppliers');

const router = express.Router();

router.get('/', getAllSuppliers);
router.get('/:id', getOneSupplier);
router.post('/', createSupplier);
router.put('/:id', updateSupplier);
router.delete('/:id', deleteSupplier);

module.exports = router;