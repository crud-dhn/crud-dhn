const express = require("express");

// Import controller
const {
  createCustomers,
  getAllCustomers,
  getOneCustomers,
  updateCustomers,
  deleteCustomers,
} = require("../controllers/customer");

const router = express.Router(); // Make router

router.get("/", getAllCustomers);
router.post("/", createCustomers);
router.get("/:id", getOneCustomers);
router.put("/:id", updateCustomers);
router.delete("/:id", deleteCustomers);

module.exports = router;