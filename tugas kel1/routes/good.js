const express = require('express');

const {
    getAllGoods,
    getOneGood,
    createGood,
    updateGood,
    deleteGood,
} = require('../controllers/good');

const router = express.Router();

router.get('/', getAllGoods);
router.get('/:id', getOneGood);
router.post('/', createGood);
router.put('/:id', updateGood);
router.delete('/:id', deleteGood);

module.exports = router;