const { query } = require("../models"); 

class Good {
  // All Goods
  async getAllGoods(req, res, next) {
    try {
      // Find all goods data and order by id good
      const data = await query("SELECT * FROM goods");

      // If data is empty array
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Good not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Get Detail
  async getOneGood(req, res, next) {
    try {
      // Find spesific good 
      const data = await query(
        "SELECT g.id, g.name, g.price, g.id_supplier FROM goods g JOIN suppliers s ON s.id= g.id_supplier WHERE g.id=?",
        [req.params.id]
      );

      // If data is empty array
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Good not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Create good
  async createGood(req, res, next) {
    try {
      

      // Insert Data
      const insertedData = await query(
        "INSERT INTO goods(name, price, id_supplier) VALUES (?, ?, ?)",
        [req.body.name, req.body.price, req.body.id_supplier]
      );

      // Find new Data
      const data = await query(
        "SELECT g.id, g.name, g.price, g.id_supplier FROM goods g JOIN suppliers s ON s.id= g.id_supplier WHERE g.id=?",
        [insertedData.insertId]
      );

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async updateGood(req, res, next) {
    try {
      // Update good data
      const updatedData = await query(
        "UPDATE goods g SET g.name=?, g.price=?, g.id_supplier=? WHERE id=?",
        [req.body.name, req.body.price, req.body.id_supplier, req.params.id]
      );

      if (updatedData.affectedRows === 0) {
        return res.status(404).json({
          errors: ["Good not found"],
        });
      }

      //   Find updated Data
      const data = await query(
        "SELECT g.id, g.name, g.price, g.id_supplier FROM goods g JOIN suppliers s ON s.id= g.id_supplier WHERE g.id=?",
        [req.params.id]
      );

      res.status(200).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async deleteGood(req, res, next) {
    try {
      const deletedData = await query("DELETE FROM goods WHERE id=?", [
        req.params.id,
      ]);

      if (deletedData.affectedRows === 0) {
        return res.status(404).json({
          errors: ["Good has been deleted or it's not exists"],
        });
      }

      res.status(200).json({ data: [] });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Good();
