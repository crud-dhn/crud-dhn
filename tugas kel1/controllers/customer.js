const { query } = require("../models");

class Customers {
  // All Customers
  async getAllCustomers(req, res, next) {
    try {
      // Find all customer data and order by id customer
      const data = await query(
        "SELECT c.id, c.name as customersName FROM customers c ORDER BY c.id"
      );

      // If data is empty array
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Customers not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Get Detail
  async getOneCustomers(req, res, next) {
    try {
      // Find spesific customer by req.params.id
      const data = await query(
        "SELECT c.id, c.name as customersName FROM customers c WHERE c.id=?",
        [req.params.id]
      );

      // If data is empty array
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Customers not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Create customer
  async createCustomers(req, res, next) {
    try {
      const customers = await query("SELECT * FROM customers WHERE id=?", [
        req.body.id,
      ]);

      // Insert Data
      const insertedData = await query(
        "INSERT INTO customers(name) VALUES (?)",
        [req.body.name]
      );

      // Find new Data
      const data = await query(
        "SELECT c.id, c.name as customersName FROM customers c WHERE c.id=?",
        [insertedData.insertId]
      );

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
  async updateCustomers(req, res, next) {
    try {
      
      const customers = await query("SELECT * FROM customers WHERE id=?", [
        req.body.id,
      ]);

      // Update customer data
      const updatedData = await query(
        "UPDATE customers SET name=? WHERE id=?",
        [req.body.name, req.params.id]
      );

      if (updatedData.affectedRows === 0) {
        return res.status(404).json({
          errors: ["Customers not found"],
        });
      }

      // Find updated Data
      const data = await query(
        "SELECT c.id, c.name as customersName FROM customers c WHERE c.id=?",
        [req.params.id]
      );

      res.status(200).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async deleteCustomers(req, res, next) {
    try {
      const deletedData = await query("DELETE FROM customers WHERE id=?", [
        req.params.id,
      ]);

      if (deletedData.affectedRows === 0) {
        return res.status(404).json({
          errors: ["Customer has been deleted or it's not exists"],
        });
      }

      res.status(200).json({ data: [] });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Customers();
