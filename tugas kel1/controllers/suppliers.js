const { query } = require("../models"); 

class Suppliers {
  // All Suppliers
  async getAllSuppliers(req, res, next) {
    try {
      // Find all suppliers data and order by id suppliers
      const data = await query("SELECT * FROM suppliers");

      // If data is empty array
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Suppliers not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Get Detail Suppliers
  async getOneSupplier (req, res, next) {
    try {
      // Find spesific supplier 
      const data = await query(
        "SELECT s.id, s.name FROM suppliers s WHERE s.id=?",
        [req.params.id]
      );

      // If data is empty array
      if (data.length === 0) {
        return res.status(404).json({ errors: ["supplier not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Create supplier
  async createSupplier(req, res, next) {
    try {
      const suppliers = await query("SELECT * FROM suppliers WHERE id=?", [
        req.body.id,
      ]);

      // Insert Data
      const insertedData = await query(
        "INSERT INTO suppliers(name) VALUES (?)",
        [req.body.name]
      );

      // Find new Data
      const data = await query(
        "SELECT s.id, s.name FROM suppliers s WHERE s.id=?",
        [insertedData.insertId]
      );

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async updateSupplier(req, res, next) {
    try {

        
      const suppliers = await query("SELECT * FROM suppliers WHERE id=?", [
        req.body.id,
      ]);
      // Update supplier data
      const updatedData = await query(
        "UPDATE suppliers s SET s.name=? WHERE id=?",
        [req.body.name, req.params.id]
      );

      if (updatedData.affectedRows === 0) {
        return res.status(404).json({
          errors: ["Suppliers not found"],
        });
      }

      //   Find updated Data
      const data = await query(
        "SELECT s.id, s.name FROM suppliers s WHERE s.id=?",
        [req.params.id]
      );

      res.status(200).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async deleteSupplier(req, res, next) {
    try {
      const deletedData = await query("DELETE FROM suppliers WHERE id=?", [
        req.params.id,
      ]);

      if (deletedData.affectedRows === 0) {
        return res.status(404).json({
          errors: ["Suppliers has been deleted or it's not exists"],
        });
      }

      res.status(200).json({ data: ["Supplier was successfully removed"] });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Suppliers();
