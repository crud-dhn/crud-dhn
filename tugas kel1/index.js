const express = require("express"); // Import express

/* Import routes */
const customers = require("./routes/customer");
const goods = require("./routes/good");
const suppliers = require("./routes/suppliers");

const port = process.env.PORT || 3000; // Define the port

const app = express(); // Make express app

/* Enable req.body */
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/* Using routes */
app.use("/customers", customers);
app.use("/goods", goods);
app.use("/suppliers", suppliers);

/* Running the app server */
app.listen(port, () => console.log(`Server running on ${port}!`));