const express = require("express");

// Import validator
const {
  createCustomersValidator,
} = require("../middlewares/validators/customers");

// Import controller
const { createCustomers } = require("../controllers/customers");

const router = express.Router();

router.route("/").post(createCustomersValidator, createCustomers);

module.exports = router;
