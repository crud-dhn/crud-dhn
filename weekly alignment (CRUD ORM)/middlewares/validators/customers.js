const path = require("path");
const validator = require("validator");
const { promisify } = require("util");

exports.createCustomersValidator = async (req, res, next) => {
  try {
    const errors = [];

    //Check there is a name of customers
    if (!validator.isEmpty(req.body.name)) {
      errors.push("The name of customer must not be empty");
    }

    // if error
    if (errors.length > 0) {
      return res.status(400).json({ errors: errors });
    }

    next();
  } catch (error) {
    res.status(500).json({ errors: ["Internet Server Error"] });
  }
};
