const express = require("express");

// Import validator
const {
  createCustomersValidator,
} = require("../middlewares/validators/customers");

// Import controller
const {
  getAllCustomer,
  getDetailCustomer,
  createCustomer,
  updateCustomer,
  deleteCustomer,
} = require("../controllers/customers");

const router = express.Router();

router
  .route("/")
  .post(createCustomersValidator, createCustomer)
  .get(getAllCustomer);

router
  .route("/:id")
  .get(getDetailCustomer)
  .put(updateCustomer)
  .delete(deleteCustomer);

module.exports = router;
