const express = require("express");

// Import validator
const {
  createTransactionsValidator,
} = require("../middlewares/validators/transactions");

// Import controller
const { createTransactions } = require("../controllers/transactions");

const router = express.Router();

router.route("/").post(createTransactionsValidator, createTransactions);

module.exports = router;
